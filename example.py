# import slack
# from quart import Quart

# app = Quart(__name__)
# bot_slack_token = "xoxb-987764685494-1315914836068-pOIsmlkzMBDja6VdF7fkLr9e"
# client = slack.WebClient(token=bot_slack_token, run_async=True)

# async def send_async_message(channel, text, client):
#     response = await client.chat_postMessage(channel=channel, text=text)

#     assert response["ok"]
#     assert response["message"]["text"] == "ok"

# @app.route("/")
# async def notify():
#     await send_async_message(channel="#test_elite_health_bot2", text="ok", client=client)
#     return "OK"

# if __name__ == "__main__":
#     app.run(debug=False)
    
import os
import slack
# from slack.errors import SlackApiError

client = slack.WebClient(
    token="xoxb-987764685494-1315914836068-pOIsmlkzMBDja6VdF7fkLr9e",
    run_async=True # turn async mode on
)
# Define this as an async function
async def send_to_slack(channel, text):
    try:
        # Don't forget to have await as the client returns asyncio.Future
        response = await client.chat_postMessage(
            channel=channel,
            text=text
        )
        assert response["message"]["text"] == text
    except SlackApiError as e:
        assert e.response["ok"] is False
        assert e.response["error"]  # str like 'invalid_auth', 'channel_not_found'
        raise e

# https://sanicframework.org/
from sanic import Sanic
from sanic.response import json

app = Sanic()
# e.g., http://localhost:3000/?text=foo&text=bar
@app.route('/')
async def test(request):
    text = 'Hello World!'
    if 'text' in request.args:
        text = "\t".join(request.args['text'])
    try:
        await send_to_slack(channel="#test_elite_health_bot2", text=text)
        return json({'message': 'Done!'})
    except SlackApiError as e:
        return json({'message': "Failed due to {e.response['error']}"})

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=3000)