import os, slackclient, time
import random
import pandas as pd
import tensorflow_hub as hub
import tensorflow as tf
import tensorflow_text
from scipy import spatial

# delay in seconds before checking for new events 
SOCKET_DELAY = 1
# slackbot environment variables
VALET_SLACK_NAME = os.environ.get('VALET_SLACK_NAME')
VALET_SLACK_TOKEN = os.environ.get('VALET_SLACK_TOKEN')
VALET_SLACK_ID = os.environ.get('VALET_SLACK_ID')
valet_slack_client = slackclient.SlackClient(VALET_SLACK_TOKEN)

embedding = "https://tfhub.dev/google/universal-sentence-encoder-multilingual-large/3"

hub_layer = hub.KerasLayer(embedding, input_shape=[], 
                           dtype=tf.string, trainable=False)

def get_plan_type(text):
    key_dict={"แผน 1-4":"plan1, plan2, plan3, plan4","แผน 1-3":"plan1, plan2, plan3",
          "แผน 1-2":"plan1, plan2", "แผน 2-4":"plan2, plan3, plan4","แผน 2-3":"plan2, plan3",
          "แผน 3-4":"plan1, plan2", "แผน 1":"plan1", "แผน 2":"plan2", "แผน 3":"plan3", "แผน 4":"plan4",
          "plan 1":"plan1","plan 2":"plan2", "plan 3":"plan3", "plan 4":"plan4"
         }
    plan_list=[]
    for key in key_dict.keys():
        if(key in text):
            text=text.replace(key,key_dict[key])
            semi_list=key_dict[key].split(",")
            plan_list=plan_list+semi_list
    plan_list=" ".join(list(set(list(plan_list))))
    return  pd.Series([text, plan_list])

qa_df=pd.read_csv('data/policy_answer.csv',header=None)
qa_df=qa_df[[1,2,3,4]]
qa_df=qa_df.drop_duplicates(subset=[1,2]).reset_index(drop=True)
qa_df.columns=['question','answer','plan_type','policy_type']
qa_df["question"]=qa_df.apply(lambda rows: " ".join(rows["question"].lower().split()),axis=1)
qa_df["answer"]=qa_df.apply(lambda rows: " ".join(rows["answer"].lower().split()),axis=1)

syn_df=pd.read_csv('data/synonym.csv',header=None)
syn_dict=dict(zip(syn_df[0], syn_df[1]))

def find_closest_args(centroids,features):
        centroid_min = 1e10
        cur_arg = -1
        args = {}
        used_idx = []

        for j, centroid in enumerate(centroids):

            for i, feature in enumerate(features):
                value=1-spatial.distance.cosine(feature, centroid)
            args[j] = value
        return args

def translate(text, conversion_dict, before=None):
    """
    Translate words from a text using a conversion dictionary

    Arguments:
        text: the text to be translated
        conversion_dict: the conversion dictionary
        before: a function to transform the input
        (by default it will to a lowercase)
    """
    # if empty:
    if not text: return text
    # preliminary transformation:
    before = before or str.lower
    t = before(text)
    for key, value in conversion_dict.items():
        t = t.replace(key, value)
    return t    

centroids=[]
for i in range(qa_df.shape[0]):
    sen1=qa_df.loc[i,"question"]
    sen1=translate(sen1.lower(), syn_dict)
    centroids.append(hub_layer(sen1))
    
def find_faq(message):
    sen1=message.strip()
    features=hub_layer(sen1)
    _,q_type=get_plan_type(sen1)
    cluster_args = find_closest_args(centroids,features)
    cluster_args_x = sorted(cluster_args.items(), key=lambda kv: -kv[1])

    for cluster in cluster_args_x:
        ans_plan_type=qa_df.loc[cluster[0],'plan_type']
        if(cluster[1]>=0.75):
            if(isinstance(ans_plan_type, str)):
                if(q_type.split(" ")[0] in ans_plan_type):
#                     print("q_re:",qa_df.loc[cluster[0],'question'])
#                     print("a_re:",qa_df.loc[cluster[0],'answer'])
#                     print("plan_type_re:",ans_plan_type)
#                     print("policy_type_re:",qa_df.loc[cluster[0],'policy_type'])
#                     print("a_score:",cluster[1])
                    
                    return True,"ref_Q:{} \nref_A:{} \nplan_type:{} \nscore:{}".format(qa_df.loc[cluster[0],'question'],qa_df.loc[cluster[0],'answer'],ans_plan_type,cluster[1])
            
            else:
#                 print("q_re:",qa_df.loc[cluster[0],'question'])
#                 print("a_re:",qa_df.loc[cluster[0],'answer'])
#                 print("plan_type_re:",ans_plan_type)
#                 print("policy_type_re:",qa_df.loc[cluster[0],'policy_type'])
#                 print("a_score:",cluster[1])
#                 print("1----------")
                
                return True,"ref_Q:{} \nref_A:{}\n score:{}".format(qa_df.loc[cluster[0],'question'],qa_df.loc[cluster[0],'answer'],cluster[1])

        #     print(cluster[1])
        if(cluster[1]>=0.5):
#             print("คำตอบที่ใกล้เคียงที่สุด")
#             print("q_re:",qa_df.loc[cluster[0],'question'])
#             print("a_re:",qa_df.loc[cluster[0],'answer'])
#             print("plan_type_re:",ans_plan_type)
#             print("policy_type_re:",qa_df.loc[cluster[0],'policy_type'])
#             print("a_score:",cluster[1])
#             print("2------------")
            
            return True,"Most related answer \nref_Q:{} \nref_A:{} \nscore:{}".format(qa_df.loc[cluster[0],'question'],qa_df.loc[cluster[0],'answer'],cluster[1])
        
        else:
#             print("ไม่พบคำตอบเพิ่มเติมใน faq สามารถ")
#             print("พิมพ์ 1 ไป ค้นหาในเอกสาร")
#             print("พิมพ์ 2 ถาม expert")
            return False,"Please contact an agent expert"
        
def is_private(event):
    """Checks if private slack channel"""
    return event.get('channel').startswith('D')

# how the bot is mentioned on slack
def get_mention(user):
    return '<@{user}>'.format(user=user)


def is_for_me(event):
    """Know if the message is dedicated to me"""
    # check if not my own event
    type = event.get('type')
    if type and type == 'message' and not(event.get('user')==VALET_SLACK_ID):
        # in case it is a private message return true
        if is_private(event):
            return True
        # in case it is not a private message check mention
        text = event.get('text')
        channel = event.get('channel')
        valet_slack_mention = get_mention(VALET_SLACK_ID)
        if valet_slack_mention in text.strip().split():
            return True

def is_hi(message):
    tokens = [word.lower() for word in message.strip().split()]
    return any(g in tokens
               for g in ['hello', 'bonjour', 'hey', 'hi', 'sup', 'morning', 'hola', 'ohai', 'yo'])

def is_bye(message):
    tokens = [word.lower() for word in message.strip().split()]
    return any(g in tokens
               for g in ['bye', 'goodbye', 'revoir', 'adios', 'later', 'cya'])

def say_hi(user_mention):
    """Say Hi to a user by formatting their mention"""
    response_template = random.choice(['Sup, {mention}...',
                                       'Yo!',
                                       'Hola {mention}',
                                       'Bonjour!'])
    return response_template.format(mention=user_mention)


def say_bye(user_mention):
    """Say Goodbye to a user"""
    response_template = random.choice(['see you later, alligator...',
                                       'adios amigo',
                                       'Bye {mention}!',
                                       'Au revoir!'])
    return response_template.format(mention=user_mention)

def handle_message(message, user, channel):
    if is_hi(message):
        user_mention = get_mention(user)
        post_message(message=say_hi(user_mention), channel=channel)
    elif is_bye(message):
        user_mention = get_mention(user)
        post_message(message=say_bye(user_mention), channel=channel)
    else:
        status,ans=find_faq(message)
        if status:
            post_message(message=ans, channel=channel)

def post_message(message, channel):
    valet_slack_client.api_call('chat.postMessage', channel=channel,
                          text=message, as_user=True)
def run():
    if valet_slack_client.rtm_connect():
        print('[.] Valet de Machin is ON...')
        print(VALET_SLACK_ID)
        while True:
            event_list = valet_slack_client.rtm_read()
            
            if len(event_list) > 0:
                for event in event_list:
                    print(event)
                    if is_for_me(event):
                        handle_message(message=event.get('text'), user=event.get('user'), channel=event.get('channel'))
                        break
            time.sleep(SOCKET_DELAY)
    else:
        print('[!] Connection to Slack failed.')

if __name__=='__main__':
    run()