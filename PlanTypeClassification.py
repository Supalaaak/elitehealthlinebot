import tensorflow as tf
import numpy as np

try:
    # Disable all GPUS
    tf.config.set_visible_devices([], 'GPU')
    visible_devices = tf.config.get_visible_devices()
    for device in visible_devices:
        assert device.device_type != 'GPU'
except:
    # Invalid device or cannot modify virtual devices once initialized.
    pass

import tensorflow_hub as hub
import tensorflow_text

embedding = "https://tfhub.dev/google/universal-sentence-encoder-multilingual-large/3"
hub_layer = hub.KerasLayer(embedding, input_shape=[], 
                           dtype=tf.string, trainable=False)

def get_model(drop_out, no_classes):
    model = tf.keras.Sequential()
    model.add(hub_layer)
    model.add(tf.keras.layers.BatchNormalization())
    model.add(tf.keras.layers.Activation('tanh'))
    model.add(tf.keras.layers.Dropout(drop_out))
#     model.add(tf.keras.layers.Dense(8, activation='relu'))
    model.add(tf.keras.layers.Dense(no_classes, activation='softmax'))
    return model

MyClassifier=None

def ElitePlanClassifier(text):
    
    global MyClassifier
    if not MyClassifier:
        MyClassifier = PlanTypeClassifier()
        
    ans_list=[]
    ans=MyClassifier.predict_general(text)
    if(ans==1):
        ans_list.append("Insurance_Policy")
        ans_list.append("general")
    
    ans=MyClassifier.predict_benefit_2_er(text)
    if(ans==1):
        ans_list.append("benefit_2_er")
        ans_list.append("exception")
        
    ans=MyClassifier.predict_benefit_1_ipd(text)
    if(ans==1):
        ans_list.append("benefit_1_ipd")
        ans_list.append("exception")
        
    ans=MyClassifier.predict_benefit_3_opd(text)
    if(ans==1):
        ans_list.append("benefit_3_opd")
        ans_list.append("exception")
        
    ans=MyClassifier.predict_exception(text)
    if(ans==1):
        ans_list.append("exception")
    
    ans=MyClassifier.predict_benefit_else(text)
    if(ans==1):
        ans_list.append("benefit_summary")
        ans_list.append("benefit_4_birth")
        ans_list.append("benefit_5_others")
        ans_list.append("exception")
    return ans_list

class PlanTypeClassifier(object):
    def __init__(self, drop_out=0.1, no_classes=2):
        self.model_general=get_model(drop_out, no_classes)
        self.model_benefit_2_er=get_model(drop_out, no_classes)
        self.model_benefit_1_ipd=get_model(drop_out, no_classes)
        self.model_benefit_3_opd=get_model(drop_out, no_classes)
        self.model_exception=get_model(drop_out, no_classes)
        self.model_benefit_else=get_model(drop_out, no_classes)
        
        self.model_general.load_weights("elite_policy_model/general_best.h5")
        self.model_benefit_2_er.load_weights("elite_policy_model/benefit_2_er_best.h5")
        self.model_benefit_1_ipd.load_weights("elite_policy_model/benefit_1_ipd_best.h5")
        self.model_benefit_3_opd.load_weights("elite_policy_model/benefit_3_opd_best.h5")
        self.model_exception.load_weights("elite_policy_model/exception_best.h5")
        self.model_benefit_else.load_weights("elite_policy_model/benefit_else_best.h5")
        
    def predict_general(self,text):
        y_s=self.model_general.predict([text])
        ans=np.asarray(y_s.argmax(axis=-1))[0]
        return ans
    
    def predict_benefit_2_er(self,text):
        y_s=self.model_benefit_2_er.predict([text])
        ans=np.asarray(y_s.argmax(axis=-1))[0]
        return ans
    
    def predict_benefit_1_ipd(self,text):
        y_s=self.model_benefit_1_ipd.predict([text])
        ans=np.asarray(y_s.argmax(axis=-1))[0]
        return ans
    
    def predict_benefit_3_opd(self,text):
        y_s=self.model_benefit_3_opd.predict([text])
        ans=np.asarray(y_s.argmax(axis=-1))[0]
        return ans
    
    def predict_exception(self,text):
        y_s=self.model_exception.predict([text])
        ans=np.asarray(y_s.argmax(axis=-1))[0]
        return ans
    
    def predict_benefit_else(self,text):
        y_s=self.model_benefit_else.predict([text])
        ans=np.asarray(y_s.argmax(axis=-1))[0]
        return ans