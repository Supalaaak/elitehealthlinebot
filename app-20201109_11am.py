#!/usr/bin/python
#-*-coding: utf-8 -*-
##from __future__ import absolute_import
###
from flask import Flask, jsonify, render_template, request
import json
import numpy as np
import copy

from linebot.models import (
    MessageEvent, TextMessage, TextSendMessage,TemplateSendMessage,ImageSendMessage, StickerSendMessage, AudioSendMessage, FlexSendMessage
)
from linebot.models.template import *
from linebot import (
    LineBotApi, WebhookHandler
)

import random
import pandas as pd
import tensorflow_hub as hub
import tensorflow as tf
import tensorflow_text
from scipy import spatial
import requests
import glob 
import base64
import datetime
import re
import transformers
import torch
from transformers import pipeline
import deepcut
from PlanTypeClassification import ElitePlanClassifier

app = Flask(__name__)

lineaccesstoken = 'PXZoRPVALeMbR1rHXkz0UVNxFXGDcyJDCGVPJ/M4oh1CxnI2WXaHmGLc+ulo9mi+95iGJpBN/DoKTjWU8ccfghFmJOcO3SkSmI2d/xo5G1qFBEPaLuP2YqTzQUdKtpaVkNNuiZcGGwau43kfHe03lwdB04t89/1O/w1cDnyilFU='
line_bot_api = LineBotApi(lineaccesstoken)

######################## hub #######################

embedding = "https://tfhub.dev/google/universal-sentence-encoder-multilingual-large/3"
hub_layer = hub.KerasLayer(embedding, input_shape=[], 
                           dtype=tf.string, trainable=False)
plan_dict={}
for plan in ['plan1','plan2','plan3','plan4','all']:
    text_files=glob.glob("elite_plan/{}/*".format(plan))
    for file in text_files:
        policy_type=file.split("/")[2].split(".")[0]
        all_text=open(file,"r").read()
        plan_dict["{}-{}".format(plan,policy_type)]=all_text

qa_pipeline = pipeline(
    "question-answering",
    model="short_token_thai",
    tokenizer="short_token_thai")

def get_line_index(context_list,idx):
    string_len=0
    for i,context in enumerate(context_list):
        string_len=string_len+len(context)
        if(idx<=string_len):
            return i
        string_len=string_len+1
    return 0

def get_plan_type(text):
    key_dict={"แผน 1-4":"plan1, plan2, plan3, plan4","แผน 1-3":"plan1, plan2, plan3",
          "แผน 1-2":"plan1, plan2", "แผน 2-4":"plan2, plan3, plan4","แผน 2-3":"plan2, plan3",
          "แผน 3-4":"plan1, plan2", "แผน 1":"plan1", "แผน 2":"plan2", "แผน 3":"plan3", "แผน 4":"plan4",
          "plan 1":"plan1","plan 2":"plan2", "plan 3":"plan3", "plan 4":"plan4",
          "แผน1":"plan1", "แผน2":"plan2", "แผน3":"plan3", "แผน4":"plan4"
         }
    plan_list=[]
    clean_text=text
    for key in key_dict.keys():
        if(key in text):
            text=text.replace(key,key_dict[key])
            clean_text=clean_text.replace(key," ")
            semi_list=key_dict[key].split(",")
            plan_list=plan_list+semi_list
    return clean_text,(list(set(list(plan_list))))

def add_star(all_text):
    content_list=all_text.split("\n")
    star_list=[">{}".format(text) for text in content_list]
    return "\n".join(star_list)

def highlight_answer(hub_id,all_text,hub_text):
    content_list=all_text.split("\n\n")
    for i in range(hub_id,len(content_list)):
        if(content_list[i] in hub_text):
            content_list[i]=add_star(content_list[i])
    return "\n\n".join(content_list)

def cdqa_text(context,question):
    answer_dict=qa_pipeline({'context': context,'question': question})
    context_list=context.split("\n")
    start_idx=answer_dict['start']
    end_idx=answer_dict['end']
    start_line=get_line_index(context_list,start_idx)
    end_line=get_line_index(context_list,end_idx)
    if(start_line==end_line):
        context_list[start_line]="*{}*".format(context_list[start_line])
    else:
        for i in range(start_line,end_line+1):
            context_list[i]="*{}*".format(context_list[i])
            
    return True,"\n".join(context_list)

def exception_or_not(sen1):
    if("พื้นที่คุ้มครอง" in sen1):
        return False
    elif("พื้นที่ความคุ้มครอง" in sen1):
        return False
    elif("คุ้มครอง" in sen1):
        return True
    elif( "หรือไม่" in sen1):
        return True
    elif( "จ่าย" in sen1):
        return True
    elif( "เบิก" in sen1):
        return True
    elif( "เท่าไหร่" in sen1):
        return True
    elif( "เท่าไร" in sen1):
        return True
    elif( "สิทธิ์" in sen1):
        return True
    elif( "รับประกัน" in sen1):
        return True
    return False

boost_words=['ไข้','ตา','ไต','บุหรี่','เลสิค','แท้ง','ต่างประเทศ','พื้นที่ความคุ้มครอง','นิ่ว','ฟัน','ทันตกรรม','เอ็กซ์เรย์','แพทย์แผนจีน','กายภาพ','เตียง']
with open('boost_words.txt', 'r') as f:
    boost_words = f.read().splitlines()
    
def check_boost_words(sen1,url):
    for word in boost_words:
        if(word in sen1):
            new_url="{}&defType=edismax&bq=text:{}^10".format(url,word)
            return new_url
    return url

def check_bypass_plan(question):
    token_question=" ".join(deepcut.tokenize(question.strip()))
    url='http://localhost:8983/solr/elite_plan2/select?fq=-policy_type:definition&fl=id,score,plan_type,policy_type,text,score&q=token_text:({})'.format(token_question) 
    url=check_boost_words(question,url)
    r = requests.get(url)
    try:
        output=json.loads(r.text)['response']['docs']
        if(len(output)>0):
            out=output[0]
            print(output[0])
            if out['plan_type']=="all":
                return True
            if out['policy_type']=="benefit_2_er":
                return True
            if out['policy_type']=="general":
                return True
            if out['policy_type']=="definition":
                return True
    except:
        return False
    return False

def knowledge_base(question):
    sen1=translate(question.lower(), syn_dict)
    sen1_vector=hub_layer(sen1)
    clean_text,plan_list= get_plan_type(question.lower())
    
    if(len(plan_list)<=0):
        if(exception_or_not(sen1)):
            url='http://localhost:8983/solr/elite_plan/select?fq=-policy_type:definition&fl=id,score,plan_type,policy_type,text,score&q=text:({})'.format(question)  
#             url='http://localhost:8983/solr/elite_plan/select?fq=-policy_type:(general or definition)&fl=id,score,plan_type,policy_type,text,score&q=text:({})'.format(question)   
        else:
            url='http://localhost:8983/solr/elite_plan/select?fl=id,score,plan_type,policy_type,text,score&q=text:({})'.format(question)
    else:
        if(exception_or_not(sen1)):
            url='''http://localhost:8983/solr/elite_plan/select?fq=plan_type:({} or all)&fq=-policy_type: definition&fl=id,score,plan_type,policy_type,text,score&q=text:({})'''.format(plan_list[0],clean_text)
#             url='''http://localhost:8983/solr/elite_plan/select?fq=plan_type:{}&fq=-policy_type:(general or definition)&fl=id,score,plan_type,policy_type,text,score&q=text:({})'''.format(plan_list[0],clean_text)
        else:
            url='http://localhost:8983/solr/elite_plan/select?fq=(plan_type:{} or all)&fl=id,score,plan_type,policy_type,text,score&q=text:({})'.format(plan_list[0],clean_text)


    url=check_boost_words(sen1,url)
#     print("---------")
#     print(url)
#     print("---------")

    r = requests.get(url)
    output=json.loads(r.text)['response']['docs']
    if(len(output)>0):
        max_value=-100
        hub_text=""
        hub_policy=""
        hub_score=0
        hub_id=0
        for out in output[:1]:
            sen2=translate(out['text'], syn_dict)
            sen2_vector=hub_layer(sen2)
            value=1-spatial.distance.cosine(sen1_vector, sen2_vector)
            if(value>=max_value):
                hub_id=int(out['id'].split("_")[-1])
                hub_text=out['text']
                hub_policy=out['policy_type']
                hub_plan_type=out['plan_type']
                hub_score=value
                max_value=value
        all_text=plan_dict["{}-{}".format(hub_plan_type,hub_policy)]
        full_text=highlight_answer(hub_id,all_text,hub_text)
        
        status,out_text=cdqa_text(hub_text,question)
        
#         if(hub_policy=="exception"):
#             new_text="*{}:อยู่ในข้อยกเว้นของสัญญา*\n{}".format(hub_policy,out_text) 
#         else:
#             new_text="*{}*\n{}".format(hub_policy,out_text)

        if(hub_policy=="exception"):
            new_text="{}:อยู่ในข้อยกเว้นของสัญญา\n\n{}".format(hub_policy,out_text) 
        else:
            new_text="{}\n\n{}".format(hub_policy,out_text)

        
        return new_text,hub_policy,hub_score,full_text
    return "","",-1,""

def knowledge_base_json(question):
    sen1=translate(question.lower(), syn_dict)
    sen1_vector=hub_layer(sen1)
    clean_text,plan_list= get_plan_type(question.lower())
    
    if(len(plan_list)<=0):
        token_question=" ".join(deepcut.tokenize(question.strip()))
        if(exception_or_not(sen1)):
            url='http://localhost:8983/solr/elite_plan2/select?fq=-policy_type:definition&fl=id,score,plan_type,policy_type,text,score&q=token_text:({})'.format(token_question)  
#             url='http://localhost:8983/solr/elite_plan/select?fq=-policy_type:(general or definition)&fl=id,score,plan_type,policy_type,text,score&q=text:({})'.format(question)   
        else:
            url='http://localhost:8983/solr/elite_plan2/select?fl=id,score,plan_type,policy_type,text,score&q=token_text:({})'.format(token_question)
    else:
        clean_text=" ".join(deepcut.tokenize(clean_text.strip()))
        if(exception_or_not(sen1)):
            url='''http://localhost:8983/solr/elite_plan2/select?fq=plan_type:({} or all)&fq=-policy_type: definition&fl=id,score,plan_type,policy_type,text,score&q=token_text:({})'''.format(plan_list[0],clean_text)
#             url='''http://localhost:8983/solr/elite_plan/select?fq=plan_type:{}&fq=-policy_type:(general or definition)&fl=id,score,plan_type,policy_type,text,score&q=text:({})'''.format(plan_list[0],clean_text)
        else:
            url='http://localhost:8983/solr/elite_plan2/select?fq=(plan_type:{} or all)&fl=id,score,plan_type,policy_type,text,score&q=token_text:({})'.format(plan_list[0],clean_text)


    url=check_boost_words(sen1,url)
    print(url)
    
    r = requests.get(url)
    output=json.loads(r.text)['response']['docs']
    if(len(output)>0):
        max_value=-100
        hub_text=""
        hub_policy=""
        hub_score=0
        hub_id=0
        for out in output[:1]:
            sen2=translate(out['text'], syn_dict)
            sen2_vector=hub_layer(sen2)
            value=1-spatial.distance.cosine(sen1_vector, sen2_vector)
            if(value>=max_value):
                hub_id=int(out['id'].split("_")[-1])
                hub_text=out['text']
                hub_policy=out['policy_type']
                hub_plan_type=out['plan_type']
                hub_score=value
                max_value=value

        status,out_dict=cdqa_text_json(hub_text,question,hub_policy)
        
        return out_dict,hub_score
    
    return {},-1

def check_boost_words_policy(sen1,url,policy_list):
    for word in boost_words:
        if(word in sen1):
            new_url="{}&rows=20&defType=edismax&bq=text:{}^10".format(url,word)
            for policy in policy_list:
                new_url="{}&bq=policy_type:{}^5".format(new_url,policy)
            return new_url
    for policy in policy_list:
        new_url="{}&rows=20&defType=edismax&bq=policy_type:{}^5".format(url,policy)
        return new_url
    return url


def knowledge_base_json3(question):
    policy_list=ElitePlanClassifier(question)
    sen1=translate(question.lower(), syn_dict)
    sen1_vector=hub_layer(sen1)
    clean_text,plan_list= get_plan_type(question.lower())
    
    if(len(plan_list)<=0):
        token_question=" ".join(deepcut.tokenize(question.strip()))
        if(exception_or_not(sen1)):
            url='http://localhost:8983/solr/elite_plan2/select?fq=-policy_type:definition&fl=id,score,plan_type,policy_type,text,score&q=token_text:({})'.format(token_question)  
#             url='http://localhost:8983/solr/elite_plan/select?fq=-policy_type:(general or definition)&fl=id,score,plan_type,policy_type,text,score&q=text:({})'.format(question)   
        else:
            url='http://localhost:8983/solr/elite_plan2/select?fl=id,score,plan_type,policy_type,text,score&q=token_text:({})'.format(token_question)
    else:
        clean_text=" ".join(deepcut.tokenize(clean_text.strip()))
        if(exception_or_not(sen1)):
            url='''http://localhost:8983/solr/elite_plan2/select?fq=plan_type:({} or all)&fq=-policy_type: definition&fl=id,score,plan_type,policy_type,text,score&q=token_text:({})'''.format(plan_list[0],clean_text)
#             url='''http://localhost:8983/solr/elite_plan/select?fq=plan_type:{}&fq=-policy_type:(general or definition)&fl=id,score,plan_type,policy_type,text,score&q=text:({})'''.format(plan_list[0],clean_text)
        else:
            url='http://localhost:8983/solr/elite_plan2/select?fq=(plan_type:{} or all)&fl=id,score,plan_type,policy_type,text,score&q=token_text:({})'.format(plan_list[0],clean_text)

    url=check_boost_words_policy(sen1,url,policy_list)
    r = requests.get(url)
    output=json.loads(r.text)['response']['docs']
    
    if(len(output)>0):
        max_value=-100
        hub_text=""
        hub_policy=""
        hub_score=0
        hub_id=0
        for out in output[:1]:
            sen2=translate(out['text'], syn_dict)
            sen2_vector=hub_layer(sen2)
            value=1-spatial.distance.cosine(sen1_vector, sen2_vector)
            if(value>=max_value):
                hub_id=int(out['id'].split("_")[-1])
                hub_text=out['text']
                hub_policy=out['policy_type']
                hub_plan_type=out['plan_type']
                hub_score=value
                max_value=value

        status,out_dict=cdqa_text_json(hub_text,question,hub_policy)
        
        return out_dict,hub_score
    
    return {},-1

qa_df=pd.read_csv('data/policy_answer2_2.csv',header=None)
qa_df=qa_df[[0,1,2,3,4,5]]
qa_df=qa_df.drop_duplicates(subset=[1,2]).reset_index(drop=True)
qa_df.columns=['code','question','answer','plan_type','policy_type','id']
qa_df["question"]=qa_df.apply(lambda rows: " ".join(rows["question"].lower().split()),axis=1)
qa_df["answer"]=qa_df.apply(lambda rows: " ".join(rows["answer"].lower().split()),axis=1)

syn_df=pd.read_csv('data/synonym.csv',header=None)
syn_dict=dict(zip(syn_df[0], syn_df[1]))

def find_closest_args(centroids,features):
    centroid_min = 1e10
    cur_arg = -1
    args = {}
    used_idx = []

    for j, centroid in enumerate(centroids):
        for i, feature in enumerate(features):
            value=1-spatial.distance.cosine(feature, centroid["vector"])
        args[centroid["id"]] = value
    
    return args

def translate(text, conversion_dict, before=None):
    """
    Translate words from a text using a conversion dictionary

    Arguments:
        text: the text to be translated
        conversion_dict: the conversion dictionary
        before: a function to transform the input
        (by default it will to a lowercase)
    """
    # if empty:
    if not text: return text
    # preliminary transformation:
    before = before or str.lower
    t = before(text)
    for key, value in conversion_dict.items():
        t = t.replace(key, value)
    return t    

centroids=[]
centroids_dict={}
for i in range(qa_df.shape[0]):
    sen1=qa_df.loc[i,"question"]
    sen1=translate(sen1.lower(), syn_dict)
    new_dict={"id":qa_df.loc[i,"id"]}
    new_dict["vector"]=hub_layer(sen1.lower())
    centroids_dict[qa_df.loc[i,"id"]]=i
    centroids.append(new_dict)

print("compute centroid done")

def count_star(score):
    if(score>=0.8):
        return "\udbc0\udcb2\udbc0\udcb2\udbc0\udcb2\udbc0\udcb2\udbc0\udcb2"
    elif(score>=0.70):
        return "\udbc0\udcb2\udbc0\udcb2\udbc0\udcb2\udbc0\udcb2"
    elif(score>=0.65):
        return "\udbc0\udcb2\udbc0\udcb2\udbc0\udcb2"
    elif(score>=0.60):
        return "\udbc0\udcb2\udbc0\udcb2"
    else:
        return "\udbc0\udcb2"

qa_format_json={}

for i in range(1,6):
    with open('json/star_{}.json'.format(i), 'r') as f:
        qa_format_json[i]=json.load(f)

ask_back_json={}
with open('json/ask_back.json', 'r') as f:
    ask_back_json=json.load(f)

call_expert_json={}
with open('json/call_expert2.json', 'r') as f:
    call_expert_json=json.load(f)

map_url_dict={}
with open('json/map_url_dict.json', 'r') as f:
    map_url_dict=json.load(f)

kb_json={}
with open('json/kb_all_json.json', 'r') as f:
    kb_json=json.load(f)

kb_plain_text_json={}
with open('json/kb_plain_text.json', 'r') as f:
    kb_plain_text_json=json.load(f)

kb_bold_text_json={}
with open('json/kb_bold_text.json', 'r') as f:
    kb_bold_text_json=json.load(f)

specific_ask_json={}
with open('json/specific_ask.json', 'r') as f:
    specific_ask_json=json.load(f)
    
def kb_pic_policy_url(policy_type):
    new_kb_json=copy.deepcopy(kb_json)
    if(map_url_dict.get(policy_type,-1)!=-1):
        url=map_url_dict[policy_type]
        new_kb_json['hero']['url']=url
        new_kb_json['hero']['action']['uri']=url
        if(policy_type=='exception'):
            new_kb_json['body']['contents'][0]['text']="ข้อยกเว้นความคุ้มครอง"
        else:
            new_kb_json['body']['contents'][0]['text']=policy_type
    return new_kb_json

def cdqa_text_json(context,question,policy_type):
    answer_dict=qa_pipeline({'context': context,'question': question})
    context_list=context.split("\n")
    start_idx=answer_dict['start']
    end_idx=answer_dict['end']
    start_line=get_line_index(context_list,start_idx)
    end_line=get_line_index(context_list,end_idx)
    
    start_context="\n".join(context_list[0:start_line])
    middle_context="\n".join(context_list[start_line:end_line+1])
    end_context="\n".join(context_list[end_line+1:])
    
    new_kb_json=kb_pic_policy_url(policy_type)
    
    if(len(start_context)>0):
        new_kb_plain_text_json=copy.deepcopy(kb_plain_text_json)
        new_kb_plain_text_json["text"]=start_context
        new_kb_json['body']['contents'].append(new_kb_plain_text_json)
 
    if(len(middle_context)>0):
        new_kb_bold_text_json=copy.deepcopy(kb_bold_text_json)
        new_kb_bold_text_json["text"]=middle_context
        new_kb_json['body']['contents'].append(new_kb_bold_text_json)
    
    if(len(end_context)>0):
        new_kb_plain_text_json=copy.deepcopy(kb_plain_text_json)
        new_kb_plain_text_json["text"]=end_context
        new_kb_json['body']['contents'].append(new_kb_plain_text_json)

    
    return True,new_kb_json

def qa_to_json(question,answer,score,data):
    new_data=copy.deepcopy(data)
    new_data["body"]["contents"][2]["text"]=question
    new_data["body"]["contents"][4]["text"]=answer
    new_data["body"]["contents"][3]["contents"][5]['text']="{}% Match".format(int(100*score))
    new_data["body"]["contents"][3]["contents"]=[new_data["body"]["contents"][3]["contents"][5]]
    return new_data

def count_star_json(question,answer,score):
    if(score>=0.8):
        data=qa_format_json[5]
        return qa_to_json(question,answer,score,data)
    elif(score>=0.70):
        data=qa_format_json[4]
        return qa_to_json(question,answer,score,data)
    elif(score>=0.65):
        data=qa_format_json[3]
        return qa_to_json(question,answer,score,data)
    elif(score>=0.60):
        data=qa_format_json[2]
        return qa_to_json(question,answer,score,data)
    else:
        data=qa_format_json[1]
        return qa_to_json(question,answer,score,data)
        
def get_filtered_centroids(sen1):
    url="http://localhost:8983/solr/elite_qa2/select?rows=5&q=question:({})".format(sen1)
#     url=check_boost_words(sen1,url)
    print(url)
    r=requests.get(url)
    
    try:
        outputs=json.loads(r.text)['response']['docs']
    except:
        return []
    filtered_centroids=[]
    for output in outputs:
        filtered_centroids.append(centroids[centroids_dict[output["id"]]])
    return filtered_centroids

psycho_words=[]
with open('psycho_words.txt', 'r') as f:
    psycho_words = f.read().splitlines()

def psycho_check(sen1):
    for word in psycho_words:
        if(word in sen1):
            new_sen=sen1.replace(word, " จิตเวช {} ".format(word))
            return new_sen
    return sen1

def find_faq(message,context):
    sen1=message.strip()
    if context.get('ask_question',-1)!=-1 and context.get('flow_type',-1)!=-1:
        if(context["flow_type"]=='qa'):
            sen1="แผน {} {}".format(message,context["ask_question"])
            
    sen1=sen1.replace("แพทย์แผนไทย","แพทย์ทางเลือก แพทย์แผนไทย")
    sen1=sen1.replace("แพทย์แผนจีน","แพทย์ทางเลือก แพทย์แผนจีน")
    sen1=sen1.replace("เลสิก","เลสิค")
    sen1=sen1.lower()

    orig_sen1=sen1.strip()
    sen1=" ".join(sen1.split())
    sen1=sen1.replace("elite health","ประกัน")
    sen1=sen1.replace("elite","ประกัน")
    sen1=sen1.replace("หมายถึง","คือ")
    sen1=sen1.strip()
    sen1=psycho_check(sen1)
#     print("xxxxxxxxxxxxx")
#     print(sen1,len(sen1),orig_sen1)
#     print("xxxxxxxxxxxxx")

    features=hub_layer(sen1)
    _,q_type=get_plan_type(sen1)
    
    filtered_centroids=get_filtered_centroids(orig_sen1)
    cluster_args = find_closest_args(filtered_centroids,features)
    above_args=[]
    below_args={}
    for key in cluster_args.keys():
        if(cluster_args[key]>0.75):
            above_args.append((key,cluster_args[key]))
        else:
            below_args[key]=cluster_args[key]
    below_args_x = sorted(below_args.items(), key=lambda kv: -kv[1])
    cluster_args_x=above_args+below_args_x
#     cluster_args_x = sorted(cluster_args.items(), key=lambda kv: -kv[1])
    
    for cluster in cluster_args_x:
        i_row=centroids_dict[cluster[0]]
        ans_plan_type=qa_df.loc[i_row,'plan_type']
        ans_text_lower= qa_df.loc[i_row,'answer'].lower()
        
        if(cluster[1]>=0.8):
            if qa_df.loc[i_row,'answer'].lower()=='table':
                output_list=[{"type":"image","output":{
                  "type": "image",
                  "originalContentUrl": "https://i.ibb.co/pb8LQjf/brochure-002.jpg",
                  "previewImageUrl": "https://i.ibb.co/pb8LQjf/brochure-002.jpg"
                    }
                             }]
                return 'end_conversation',{},output_list
            
        if(cluster[1]<0.75):
            break
       
            
        if(cluster[1]>=0.75 and ans_text_lower!='table'):
            
            if(isinstance(ans_plan_type, str) and len(q_type)>0 ):
                if(q_type[0] in ans_plan_type or ans_plan_type=='all'):
                    question=qa_df.loc[i_row,'question']
                    answer=qa_df.loc[i_row,'answer']
                    score=cluster[1]
                    output_list=[{"type":"carousel","output":[count_star_json(question,answer,score)]}]
                    return 'end_conversation',{},output_list
            
            elif(len(q_type)<=0 and ans_plan_type=='all'):
                question=qa_df.loc[i_row,'question']
                answer=qa_df.loc[i_row,'answer']
                score=cluster[1]
                output_list=[{"type":"carousel","output":[count_star_json(question,answer,score)]}]
                return 'end_conversation',{},output_list
            
            elif(ans_plan_type=='all'):
                question=qa_df.loc[i_row,'question']
                answer=qa_df.loc[i_row,'answer']
                score=cluster[1]
                output_list=[{"type":"carousel","output":[count_star_json(question,answer,score)]}]
                return 'end_conversation',{},output_list

            else:
                output_list=[{"type":"flex","output":ask_back_json}]
                new_context={"ask_question":message,"flow_type":"qa"}
                return 'wait',new_context,output_list
    
    if(len(q_type)<=0):
        if(not(check_bypass_plan(message))):
            output_list=[{"type":"flex","output":ask_back_json}]
            new_context={"ask_question":message,"flow_type":"qa"}
            return 'wait',new_context,output_list
    
    sen1=sen1.replace("day case","การผ่าตัดหรือหัตถการแบบผู้ป่วยนอก")
    sen1=sen1.replace("admit","เข้ารับการรักษาในโรงพยาบาล")
#     sen1=sen1.replace("fax claim","คืนที่สำรองจ่าย")
    sen1=sen1.replace("เคลม","ได้คุ้มครอง")
    sen1=sen1.replace("ทำกิ๊ฟ","การแก้ไขปัญหาการมีบุตร ทำกิ๊ฟ")
    sen1=sen1.replace("trastuzumab","รักษาโรคมะเร็งรวมถึงเคมีบําบัด trastuzumab")
    sen1=sen1.replace("มุ่งเป้า","(Targeted therapy) ออกฤทธิ์จำเพาะเจาะจงต่อเซลล์มะเร็ง")
    sen1=sen1.replace("nipt"," ผลประโยชน์การคลอดบุตร NIPT ")
    
    
    kb_hub_dict,kb_hub_score=knowledge_base_json3(sen1)
    print("hub_score",kb_hub_score)
    
    answer_list=[]
    count=0
    for cluster in cluster_args_x:
        i_row=centroids_dict[cluster[0]]
        ans_plan_type=qa_df.loc[i_row,'plan_type']
        #     print(cluster[1])
        if count>2:
            break
            
        if(cluster[1]>=0.65):
            ans_q=qa_df.loc[i_row,'question']
            ans_a=qa_df.loc[i_row,'answer']
            ans_c=qa_df.loc[i_row,'code']

            if(ans_a.lower()!="table"):
                ans_s=cluster[1]
                answer_list.append(count_star_json(ans_q,ans_a,ans_s))
#                 answer_text="ref_Q:{} \nref_A:{} \ncode:{} \nscore:{}\n{}".format(ans_q,ans_a,ans_c,ans_s,count_star(ans_s))
#                 answer_list.append(answer_text)
                count=count+1
        else:
            break
    
    if(len(answer_list)>0 and kb_hub_score>=0.1):
        output_text=[{"type":"carousel","output":[kb_hub_dict]},{"type":"carousel","output":answer_list}]
        return 'end_conversation',{},output_text

    if(len(answer_list)>0):
        output_text=[{"type":"carousel","output":answer_list}]
        return 'end_conversation',{},output_text
                      
    if(kb_hub_score>=0.1):
        output_text=[{"type":"carousel","output":[kb_hub_dict]}]
        return 'end_conversation',{},output_text
    
    output_list=[{"type":"flex","output":call_expert_json}]
#     output_text="Please contact an agent expert."
    return 'end_conversation',{},output_list

def is_hi(message):
    tokens = [word.lower() for word in message.strip().split()]
    return any(g in tokens
               for g in ['hello', 'bonjour', 'hey', 'hi', 'sup', 'morning', 'hola', 'ohai', 'yo'])

def is_bye(message):
    tokens = [word.lower() for word in message.strip().split()]
    return any(g in tokens
               for g in ['bye', 'goodbye', 'revoir', 'adios', 'later', 'cya'])

def say_hi(user_mention):
    """Say Hi to a user by formatting their mention"""
#     response_template = random.choice(['Sup, {mention}...',
#                                        'Yo!',
#                                        'Hola {mention}',
#                                        'Bonjour!'])
    response_template = random.choice(['สวัสดีค่ะ'])
    return response_template.format(mention=user_mention)


def say_bye(user_mention):
    """Say Goodbye to a user"""
    response_template = random.choice(['see you later, alligator...',
                                       'adios amigo',
                                       'Bye {mention}!',
                                       'Au revoir!'])
    return response_template.format(mention=user_mention)

def handle_message(message, context):
    if is_hi(message):
        user_mention =""
        output=say_hi(user_mention)
        output_list=[{"type":"text","output":output}]
#         output_list=[{"type":"text","output":output},{"type":"flex","output":specific_ask_json}]
        return context,'end_conversation',output_list
    elif is_bye(message):
        user_mention = ""
        output=say_bye(user_mention)
        output_list=[{"type":"text","output":output}]
        return context,'end_conversation',output_list
    else:
        current_action,context,ans=find_faq(message,context)
#         if ans=='table':
#                 post_picture(message=ans, channel=channel)
#         else:
#                 post_message(message=ans, channel=channel)
        return context,current_action,ans
####################### new ########################
@app.route('/')
def index():
    return "Hello World!"


@app.route('/webhook', methods=['POST'])
def callback():
    json_line = request.get_json(force=False,cache=False)
    json_line = json.dumps(json_line)
    decoded = json.loads(json_line)
    no_event = len(decoded['events'])
    for i in range(no_event):
        event = decoded['events'][i]
        event_handle(event)
    return '',200


# data2 = {
#   "type": "carousel",
#   "contents": []
# }
# with open('json/four_star.json', 'r') as f:
#     data2=json.load(f)

user_input = ''
context = {}
current_action = ''
follow_ind = 0
session_df = pd.DataFrame({},columns=['timestamp', 'user', 'context'])

def event_handle(event):
    
    global session_df
    
    print(event)
    
    try:
        userId = event['source']['userId']
    except:
        print('error cannot get userId')
        return ''

    try:
        rtoken = event['replyToken']
    except:
        print('error cannot get rtoken')
        return ''
    try:
        msgId = event["message"]["id"]
        msgType = event["message"]["type"]
    except:
        print('error cannot get msgID, and msgType')
        sk_id = np.random.randint(1,17)
        replyObj = StickerSendMessage(package_id=str(1),sticker_id=str(sk_id))
        line_bot_api.reply_message(rtoken, replyObj)
        return ''

    if msgType == "text":
        msg = str(event["message"]["text"])
        
        try:
            context = json.loads(session_df.loc[session_df.user == userId,'context'].values[0])
        except:
            context = {}
            start_timestamp = event["timestamp"]
            session_df = session_df.append({'timestamp': start_timestamp, 'user':userId, 'context': json.dumps(context)}, ignore_index=True)
        
        context,current_action,output=handle_message(msg,context)
        session_df.loc[session_df.user == userId,'context'] = json.dumps(context)
        
        if current_action=="wait":
            flex_message = FlexSendMessage(alt_text='hello',contents=ask_back_json)
            line_bot_api.push_message(userId, flex_message)
        else:
#             replyObj=[]
            for out_dict in output:
               if out_dict["type"]=="text":
                   text_message=TextSendMessage(text=out_dict["output"])
                   line_bot_api.push_message(userId, text_message)
               elif out_dict["type"]=="flex":
                   flex_message = FlexSendMessage(alt_text='hello',contents=out_dict["output"])
                   line_bot_api.push_message(userId, flex_message)
               elif out_dict["type"]=="image":
                   image_message = ImageSendMessage(
                                    original_content_url='https://i.ibb.co/pb8LQjf/brochure-002.jpg',
                                    preview_image_url='https://i.ibb.co/pb8LQjf/brochure-002.jpg')
                   line_bot_api.push_message(userId, image_message)
               else:
                   data2 = {"type": "carousel","contents":out_dict["output"]}   
                   flex_message = FlexSendMessage(alt_text='hello',contents=data2)
                   line_bot_api.push_message(userId, flex_message)

#             for text in output.split("\n\n"):
#                 new_text=text.strip()
#                 if(len(new_text)>0):
#                     replyObj.append(TextSendMessage(text=text))
#     #         replyObj = [TextSendMessage(text=msg),TextSendMessage(text="May I help you?")]
#             #line_bot_api.reply_message(rtoken, replyObj)
#     #         print(replyObj)
#     #         replyObj = [TextSendMessage(text=output)]
#             line_bot_api.push_message(userId, replyObj)
            
#             flex_message = FlexSendMessage(alt_text='hello',contents=data2)
#             line_bot_api.push_message(userId, flex_message)
            
        if current_action == 'end_conversation': # Based on the this, the context variables are reset
                session_df = session_df[session_df.user != userId]
                context = {}
                current_action = ''
    
#     else:
#         sk_id = np.random.randint(1,17)
#         replyObj = StickerSendMessage(package_id=str(1),sticker_id=str(sk_id))
#         line_bot_api.reply_message(rtoken, replyObj)
    return ''

if __name__ == '__main__':
    app.run(host='0.0.0.0',debug=True)
