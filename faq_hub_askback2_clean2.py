import os, time
import slackclient
import random
import pandas as pd
import tensorflow_hub as hub
import tensorflow as tf
import tensorflow_text
from scipy import spatial
import requests
import json
import glob 
import base64
import datetime
import re

# delay in seconds before checking for new events 
SOCKET_DELAY = 1
# slackbot environment variables
VALET_SLACK_NAME = os.environ.get('VALET_SLACK_NAME')
VALET_SLACK_TOKEN = os.environ.get('VALET_SLACK_TOKEN')
VALET_SLACK_ID = os.environ.get('VALET_SLACK_ID')
valet_slack_client = slackclient.SlackClient(VALET_SLACK_TOKEN)



embedding = "https://tfhub.dev/google/universal-sentence-encoder-multilingual-large/3"
hub_layer = hub.KerasLayer(embedding, input_shape=[], 
                           dtype=tf.string, trainable=False)
plan_dict={}
for plan in ['plan1','plan2','plan3','plan4']:
    text_files=glob.glob("elite_plan/{}/*".format(plan))
    for file in text_files:
        policy_type=file.split("/")[2].split(".")[0]
        all_text=open(file,"r").read()
        plan_dict["{}-{}".format(plan,policy_type)]=all_text
        
def get_plan_type(text):
    key_dict={"แผน 1-4":"plan1, plan2, plan3, plan4","แผน 1-3":"plan1, plan2, plan3",
          "แผน 1-2":"plan1, plan2", "แผน 2-4":"plan2, plan3, plan4","แผน 2-3":"plan2, plan3",
          "แผน 3-4":"plan1, plan2", "แผน 1":"plan1", "แผน 2":"plan2", "แผน 3":"plan3", "แผน 4":"plan4",
          "plan 1":"plan1","plan 2":"plan2", "plan 3":"plan3", "plan 4":"plan4",
          "แผน1":"plan1", "แผน2":"plan2", "แผน3":"plan3", "แผน4":"plan4"
         }
    plan_list=[]
    clean_text=text
    for key in key_dict.keys():
        if(key in text):
            text=text.replace(key,key_dict[key])
            clean_text=clean_text.replace(key," ")
            semi_list=key_dict[key].split(",")
            plan_list=plan_list+semi_list
    return clean_text,(list(set(list(plan_list))))

def add_star(all_text):
    content_list=all_text.split("\n")
    star_list=[">{}".format(text) for text in content_list]
    return "\n".join(star_list)

def highlight_answer(hub_id,all_text,hub_text):
    content_list=all_text.split("\n\n")
    for i in range(hub_id,len(content_list)):
        if(content_list[i] in hub_text):
            content_list[i]=add_star(content_list[i])
    return "\n\n".join(content_list)

def exception_or_not(sen1):
    if("คุ้มครอง" in sen1):
        return True
    elif( "หรือไม่" in sen1):
        return True
    return False

boost_words=['นิ่ว','เอ็กซ์เรย์','แพทย์แผนจีน','รอคอย','กายภาพ','เตียง']

def check_boost_words(sen1,url):
    for word in boost_words:
        if(word in sen1):
            new_url="{}&defType=edismax&bq=text:{}^10".format(url,word)
            return new_url
    return url

def knowledge_base(question):
    sen1=translate(question.lower(), syn_dict)
    sen1_vector=hub_layer(sen1)
    clean_text,plan_list= get_plan_type(question.lower())
    
    if(len(plan_list)<=0):
        if(exception_or_not(sen1)):
            url='http://localhost:8983/solr/elite_plan/select?fq=-policy_type:(general or definition)&fl=id,score,plan_type,policy_type,text,score&q=text:({})'.format(question)   
        else:
            url='http://localhost:8983/solr/elite_plan/select?fl=id,score,plan_type,policy_type,text,score&q=text:({})'.format(question)
    else:
        if(exception_or_not(sen1)):
            url='''http://localhost:8983/solr/elite_plan/select?fq=plan_type:{}&fq=-policy_type:(general or definition)&fl=id,score,plan_type,policy_type,text,score&q=text:({})'''.format(plan_list[0],clean_text)
        else:
            url='http://localhost:8983/solr/elite_plan/select?fq=plan_type:{}&fl=id,score,plan_type,policy_type,text,score&q=text:({})'.format(plan_list[0],clean_text)


    url=check_boost_words(sen1,url)

    r = requests.get(url)
    output=json.loads(r.text)['response']['docs']
    if(len(output)>0):
        max_value=-100
        hub_text=""
        hub_policy=""
        hub_score=0
        hub_id=0
        for out in output[:1]:
            sen2=translate(out['text'], syn_dict)
            sen2_vector=hub_layer(sen2)
            value=1-spatial.distance.cosine(sen1_vector, sen2_vector)
            if(value>=max_value):
                hub_id=int(out['id'].split("_")[-1])
                hub_text=out['text']
                hub_policy=out['policy_type']
                hub_plan_type=out['plan_type']
                hub_score=value
                max_value=value
        all_text=plan_dict["{}-{}".format(hub_plan_type,hub_policy)]
        full_text=highlight_answer(hub_id,all_text,hub_text)
        
        new_text="*{}*\n{}".format(hub_policy,hub_text)
        
        return new_text,hub_policy,hub_score,full_text
    return "","",-1,""

qa_df=pd.read_csv('data/policy_answer.csv',header=None)
qa_df=qa_df[[0,1,2,3,4]]
qa_df=qa_df.drop_duplicates(subset=[1,2]).reset_index(drop=True)
qa_df.columns=['code','question','answer','plan_type','policy_type']
qa_df["question"]=qa_df.apply(lambda rows: " ".join(rows["question"].lower().split()),axis=1)
qa_df["answer"]=qa_df.apply(lambda rows: " ".join(rows["answer"].lower().split()),axis=1)

syn_df=pd.read_csv('data/synonym.csv',header=None)
syn_dict=dict(zip(syn_df[0], syn_df[1]))

def find_closest_args(centroids,features):
        centroid_min = 1e10
        cur_arg = -1
        args = {}
        used_idx = []

        for j, centroid in enumerate(centroids):

            for i, feature in enumerate(features):
                value=1-spatial.distance.cosine(feature, centroid)
            args[j] = value
        return args

def translate(text, conversion_dict, before=None):
    """
    Translate words from a text using a conversion dictionary

    Arguments:
        text: the text to be translated
        conversion_dict: the conversion dictionary
        before: a function to transform the input
        (by default it will to a lowercase)
    """
    # if empty:
    if not text: return text
    # preliminary transformation:
    before = before or str.lower
    t = before(text)
    for key, value in conversion_dict.items():
        t = t.replace(key, value)
    return t    

centroids=[]
for i in range(qa_df.shape[0]):
    sen1=qa_df.loc[i,"question"]
    sen1=translate(sen1.lower(), syn_dict)
    centroids.append(hub_layer(sen1))

#context={'ask_question':xxx,'flow_type':qa}
def find_faq(message,context):
    sen1=message.strip()
    if context.get('ask_question',-1)!=-1 and context.get('flow_type',-1)!=-1:
        if(context["flow_type"]=='qa'):
            sen1="แผน {} {}".format(message,context["ask_question"])
    sen1=sen1.lower()
    features=hub_layer(sen1)
    _,q_type=get_plan_type(sen1)
    cluster_args = find_closest_args(centroids,features)
    cluster_args_x = sorted(cluster_args.items(), key=lambda kv: -kv[1])

    for cluster in cluster_args_x:
        ans_plan_type=qa_df.loc[cluster[0],'plan_type']
        if qa_df.loc[cluster[0],'answer'].lower()=='table':
                return 'end_conversation',{},"table"
        if(cluster[1]<0.75):
            break
            
        if(cluster[1]>=0.75):
            
            if(isinstance(ans_plan_type, str) and len(q_type)>0 ):
                if(q_type[0] in ans_plan_type):
                    
                    return 'end_conversation',{},"ref_Q:{} \nref_A:{} \nplan_type:{} \ncode:{} \nscore:{}".format(qa_df.loc[cluster[0],'question'],qa_df.loc[cluster[0],'answer'],ans_plan_type,qa_df.loc[cluster[0],'code'],cluster[1])
            
            elif(len(q_type)<=0 and not(isinstance(ans_plan_type, str))):
                return 'end_conversation',{},"ref_Q:{} \nref_A:{} \nplan_type:{} \ncode:{} \nscore:{}".format(qa_df.loc[cluster[0],'question'],qa_df.loc[cluster[0],'answer'],ans_plan_type,qa_df.loc[cluster[0],'code'],cluster[1])
            
            elif(not(isinstance(ans_plan_type, str))):
                return 'end_conversation',{},"ref_Q:{} \nref_A:{} \nplan_type:{} \ncode:{} \nscore:{}".format(qa_df.loc[cluster[0],'question'],qa_df.loc[cluster[0],'answer'],ans_plan_type,qa_df.loc[cluster[0],'code'],cluster[1])

            else:
                new_context={"ask_question":message,"flow_type":"qa"}
                return 'wait',new_context,"Which plan? \nWe have an answer for {} \nref_Q:{} \nref_A:{} \ncode:{} \nscore:{}".format(ans_plan_type,qa_df.loc[cluster[0],'question'],qa_df.loc[cluster[0],'answer'],qa_df.loc[cluster[0],'code'],cluster[1])
    
    answer_list=[]
    count=0
    for cluster in cluster_args_x:
        ans_plan_type=qa_df.loc[cluster[0],'plan_type']
        #     print(cluster[1])
        if count>2:
            break
            
        if(cluster[1]>=0.5):
            ans_q=qa_df.loc[cluster[0],'question']
            ans_a=qa_df.loc[cluster[0],'answer']
            ans_c=qa_df.loc[cluster[0],'code']

            if(ans_a.lower()!="table"):
                ans_s=cluster[1]
                answer_text="ref_Q:{} \nref_A:{} \ncode:{} \nscore:{} \n".format(ans_q,ans_a,ans_c,ans_s)
                answer_list.append(answer_text)
                count=count+1
        else:
            break
    
    if(len(answer_list)>0):
        new_context={"ask_question":message,"flow_type":"kb","state":0}
        output_text="Most related \n{} \nPlease contact an agent expert \n Try our Knowledge base?\n".format("\n".join(answer_list))
        return "wait",new_context,output_text
    
    new_context={"ask_question":message,"flow_type":"kb","state":0}
    output_text="Please contact an agent expert \n Try our Knowledge base?"
    return "wait",new_context,output_text

def is_kb(message,context):
    if context.get('flow_type',-1)!=-1:
        if(context["flow_type"]=='kb'):
            return True
    return False

def find_kb(message,context):
    sen1=re.sub(r'\<[^)]*\>', '', message)
    sen1=sen1.lower().strip()
    print("---------")
    print(sen1)
    
    if context["state"]==0:
        if(sen1=="y" or sen1=="yes" or "yes" in sen1):
            ask_question=context["ask_question"]
            _,q_type=get_plan_type(ask_question)
            if(len(q_type)>0):
                hub_text,hub_policy,hub_score,full_plan=knowledge_base(ask_question)
                if(hub_score!=-1):
                    hub_text=add_star(hub_text)
                    return 'end_conversation',{},"You ask:{} \nKnowledge base \n{} \nhub_score:{}".format(ask_question,hub_text,hub_score)
                else:
                    return 'end_conversation',{},"It's beyond my ability to answer your question. Please contact an agent expert."
            else:
                new_context=context.copy()
                new_context["state"]=1
                return 'wait',new_context,"Which plan? (Type 1,2,3 or 4)"
            
    elif(context["state"]==1):
        if(sen1 in ["1","2","3","4"]):
            ask_question=context["ask_question"]
            new_sen="แผน {} {}".format(sen1,ask_question)
            hub_text,hub_policy,hub_score,full_plan=knowledge_base(new_sen)
            if(hub_score!=-1):
                hub_text=add_star(hub_text)
                return 'end_conversation',{},"You ask:{} \nKnowledge base \n{} \nhub_score:{}".format(ask_question,hub_text,hub_score)
            else:
                return 'end_conversation',{},"It's beyond my ability to answer your question. Please contact an agent expert."
        else:
            new_context=context.copy()
            new_context["state"]=2
            return 'wait',new_context,"Which plan again? (Type 1,2,3 or 4)"
    
    elif(context["state"]==2):
        if(sen1 in ["1","2","3","4"]):
            ask_question=context["ask_question"]
            new_sen="แผน {} {}".format(sen1,ask_question)
            hub_text,hub_policy,hub_score,full_plan=knowledge_base(new_sen)
            if(hub_score!=-1):
                hub_text=add_star(hub_text)
                return 'end_conversation',{},"You ask:{} \nKnowledge base \n{} \nhub_score:{}".format(ask_question,hub_text,hub_score)
            else:
                return 'end_conversation',{},"It's beyond my ability to answer your question. Please contact an agent expert."
        else:
            return 'end_conversation',{},"table"    
    
    return 'end_conversation',{},"It's beyond my ability to answer your question. Please contact an agent expert."
                
#             hub_text,hub_policy,hub_score,full_plan=knowledge_base(sen1)
#             hub_text=add_star(hub_text)
#             if(hub_score!=-1):
#                 return 'end_conversation',{},"Most related answer \nref_Q:{} \nref_A:{} \nscore:{} \nKnowledge base \n{} \nhub_score:{}".format(qa_df.loc[cluster[0],'question'],qa_df.loc[cluster[0],'answer'],cluster[1],hub_text,hub_score)
#             else:
#                 return 'end_conversation',{},"Most related answer \nref_Q:{} \nref_A:{} \nscore:{} \nKnowledge base \n{} \nhub_score:{}".format(qa_df.loc[cluster[0],'question'],qa_df.loc[cluster[0],'answer'],cluster[1],hub_text,hub_score)
        
#         else:
#             hub_text,hub_policy,hub_score,full_plan=knowledge_base(sen1)
#             hub_text=add_star(hub_text)
#             return 'end_conversation',"Please contact an agent expert \nKnowledge base \n{} \nhub_score:{}".format(hub_text,hub_score)
        
def is_private(event):
    """Checks if private slack channel"""
    return event.get('channel').startswith('D')

# how the bot is mentioned on slack
def get_mention(user):
    return '<@{user}>'.format(user=user)
    

def is_for_me(event):
    """Know if the message is dedicated to me"""
    # check if not my own event
    type = event.get('type')
    if type and type == 'message' and not(event.get('user')==VALET_SLACK_ID):
        # in case it is a private message return true
        if is_private(event):
            return True
        # in case it is not a private message check mention
        text = event.get('text')
        channel = event.get('channel')
        valet_slack_mention = get_mention(VALET_SLACK_ID)
        if valet_slack_mention in text.strip().split():
            return True

def is_hi(message):
    tokens = [word.lower() for word in message.strip().split()]
    return any(g in tokens
               for g in ['hello', 'bonjour', 'hey', 'hi', 'sup', 'morning', 'hola', 'ohai', 'yo'])

def is_bye(message):
    tokens = [word.lower() for word in message.strip().split()]
    return any(g in tokens
               for g in ['bye', 'goodbye', 'revoir', 'adios', 'later', 'cya'])

def say_hi(user_mention):
    """Say Hi to a user by formatting their mention"""
    response_template = random.choice(['Sup, {mention}...',
                                       'Yo!',
                                       'Hola {mention}',
                                       'Bonjour!'])
    return response_template.format(mention=user_mention)


def say_bye(user_mention):
    """Say Goodbye to a user"""
    response_template = random.choice(['see you later, alligator...',
                                       'adios amigo',
                                       'Bye {mention}!',
                                       'Au revoir!'])
    return response_template.format(mention=user_mention)

def handle_message(message, user, channel,context):
    if is_hi(message):
        user_mention = get_mention(user)
        post_message(message=say_hi(user_mention), channel=channel)
        return context,'end_conversation'
    elif is_bye(message):
        user_mention = get_mention(user)
        post_message(message=say_bye(user_mention), channel=channel)
        return context,'end_conversation'

    elif is_kb(message,context):
        current_action,context,ans=find_kb(message,context)
        if ans=='table':
            post_picture(message=ans, channel=channel)  
        else:
            post_message(message=ans, channel=channel)
        return context,current_action
    else:
        current_action,context,ans=find_faq(message,context)
        if ans=='table':
                post_picture(message=ans, channel=channel)
        else:
                post_message(message=ans, channel=channel)
        return context,current_action

            
def post_message(message, channel):
    valet_slack_client.api_call('chat.postMessage', channel=channel,
                          text=message, as_user=True)

def pureimg(data1):
        data1 = '[{"text": "", "image_url": "'+data1+'"}]'
        data1 = [json.loads(data1[1:-1])]
        return data1

# https://stackoverflow.com/questions/52417750/how-to-send-image-to-slack-as-slack-bot-via-slackclient

    
payoff=os.path.join(os.path.dirname(os.path.realpath(__file__)), 'photo/brochure_002.jpg')
#It gives cross OS compatibility on filepath.
# from slackclient import SlackClient
# sc = SlackClient(SLACK_TOKEN)
# sc.api_call(
#   'files.upload', 
#   channels='#slacktesting', 
#   as_user=True, 
#   filename='pic.jpg', 
#   file=open('puppy.jpg', 'rb'),
# )
def post_picture(message, channel):
    valet_slack_client.api_call('files.upload', channels=channel, as_user=True,filename='photo/brochure_002.jpg', file=open('photo/brochure_002.jpg', 'rb'))
    
#     response=valet_slack_client.files_upload(channel=channel,file=payoff)
#     payoff=response['file']['permalink']
#     response=valet_slack_client.chat_postMessage(channel='#channel_name', text="Sample Text", username='Bot name', attachments=pureimg(payoff), icon_emoji=':emoji:')
#     response = valet_slack_client.api_call("files.upload", files={
#             'file': pureimg(payoff),
#         }, data={
#             'channels': channel,
#             'filename': 'brochure_002.jpg',
#             'title': 'brochure_002',
#             'initial_comment': 'brochure_002',
#         })
# response=valet_slack_client.chat_postMessage(channel='#channel_name', text="Sample Text", username='Bot name', attachments=pureimg(payoff), icon_emoji=':emoji:')

#     with open('photo/brochure_002.jpg', 'rb') as att:
#         r = valet_slack_client.api_call("files.upload", files={
#             'file': base64.decodebytes(att),
#         }, data={
#             'channels': channel,
#             'filename': 'brochure_002.jpg',
#             'title': 'brochure_002',
#             'initial_comment': 'brochure_002',
#         })
#         assert r.status_code == 200
#     valet_slack_client.api_call('chat.postMessage', channel=channel,
#                           text=message, as_user=True, attachments=pureimg(payoff), icon_emoji=':emoji:')
user_input = ''
context = {}
current_action = ''
follow_ind = 0
session_df = pd.DataFrame({},columns=['timestamp', 'user', 'context']) 

def run():
    global session_df
    if valet_slack_client.rtm_connect():
        print('[.] Valet de Machin is ON...')
        print(VALET_SLACK_ID)
        while True:
            event_list = valet_slack_client.rtm_read()
            
            if len(event_list) > 0:
                for event in event_list:
                    print(event)
                    if is_for_me(event):
                        message_user=event.get('user')
                        message=event.get('text')
                        channel=event.get('channel')
                        
                        if message: # If a User has typed something in Slack
                            try:
                                context = json.loads(session_df.loc[session_df.user == message_user+channel,'context'].values[0])
                            except:
                                context = {}
                                start_timestamp = datetime.datetime.fromtimestamp(float(event['event_ts'])).strftime('%Y-%m-%d %H:%M:%S')
                                session_df = session_df.append({'timestamp': start_timestamp, 'user': message_user+channel, 'context': json.dumps(context)}, ignore_index=True)
                            context,current_action=handle_message(message,message_user,channel,context)
                            session_df.loc[session_df.user == message_user+channel,'context'] = json.dumps(context)
                            if current_action == 'end_conversation': # Based on the this, the context variables are reset
                                session_df = session_df[session_df.user != message_user+channel]
                                context = {}
                                current_action = ''
                            break
            time.sleep(SOCKET_DELAY)
    else:
        print('[!] Connection to Slack failed.')

if __name__=='__main__':
    run()