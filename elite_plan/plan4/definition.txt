คำนิยาม

อุบัติเหตุ หมายถึง เหตุการณ์ที่เกิดขึ้นอย่างฉับพลันจากปัจจัยภายนอกร่างกาย และทําให้เกิดผลที่ผู้เอาประกันภัยมิได้เจตนาหรือมุ่งหวัง 

การบาดเจ็บ หมายถึง การบาดเจ็บทางร่างกาย อันเป็นผลโดยตรงจากอุบัติเหตุซึ่งเกิดขึ้น โดยเอกเทศและโดยอิสระจากเหตุอื่น

การเจ็บป่วย หมายถึง อาการ ความผิดปกติ การป่วยไข้ หรือการเกิดโรคที่เกิดขึ้นกับผู้เอาประกันภัย

แพทย์ หมายถึง ผู้ที่สําเร็จการศึกษาได้รับปริญญาแพทยศาสตร์บัณฑิต ได้ขึ้นทะเบียนอย่างถูกต้องจากแพทยสภา และได้รับอนุญาตให้ประกอบวิชาชีพ สาขาเวชกรรมในท้องถิ่นที่ให้บริการทางการแพทย์หรือทางด้านศัลยกรรม

พยาบาล หมายถึง ผู้ที่ได้รับใบอนุญาตประกอบวิชาชีพพยาบาลตามกฎหมาย 

ค่าบริการพยาบาล หมายถึง ค่าใช้จ่ายที่โรงพยาบาล คิดเป็นประจําสําหรับการให้บริการโดยพยาบาลวิชาชีพที่ให้การบริการผู้เอาประกันภัยขณะที่เป็นผู้ป่วยใน 

ผู้ป่วยใน หมายถึง ผู้ที่จําเป็นต้องเข้ารับการรักษาในโรงพยาบาล ติดต่อกันไม่น้อยกว่า 6 ชั่วโมง ซึ่งต้องลงทะเบียนเป็นผู้ป่วยใน โดยได้รับการวินิจฉัยและ คําแนะนําจากแพทย์ตามข้อบ่งชี้ ซึ่งเป็นมาตรฐานทางการแพทย์ และ ในระยะเวลาที่เหมาะสมสําหรับการรักษาการบาดเจ็บ หรือ การเจ็บป่วยนั้นๆ และให้รวมถึงกรณีรับตัวไว้เป็นผู้ป่วยในแล้วต่อมาเสียชีวิตก่อนครบ 6 ชั่วโมง

ผู้ป่วยนอก หมายถึง ผู้ที่รับบริการอันเนื่องจากการรักษาพยาบาลในแผนกผู้ป่วยนอก หรือในห้องรักษาฉุกเฉินของโรงพยาบาล ซึ่งไม่มีอว่ามจาเป็นไม่ ข้อวินิจฉัยและข้อบ่งชี้ซึ่งเป็นมาตรฐานทางการแพทย์ในการเข้ารักษาเป็นผู้ป่วยใน

โรงพยาบาล หมายถึง สถานพยาบาลใดๆ ซึ่งจัดให้บริการทางการแพทย์โดยสามารถรับผู้ป่วยไว้ค้างคืนและมีองค์ประกอบข้างด้านสถานที่มีจํานวนบุคลากร ทางการแพทย์ที่เพียงพอตลอดจนการจัดการให้บริการที่ครบถ้วน โดยเฉพาะอย่างยิ่งมีห้องสําหรับการผ่าตัดใหญ่ และได้รับอนุญาตให้ จดทะเบียนดําเนินการเป็น “โรงพยาบาล” ตามกฎหมายสถานพยาบาลของอาณาเขตนั้นๆ 

สถานพยาบาลเวชกรรม หมายถึง สถานพยาบาลใดๆ ซึ่งจัดให้บริการทางการแพทย์โดยสามารถรับผู้ป่วยไว้ค้างคืน และ ได้รับอนุญาตให้จดทะเบียนดําเนินการเป็นสถานพยาบาลเวชกรรมตามกฎหมายของอาณาเขตนั้นๆ 

คลินิก หมายถึง สถานพยาบาลแผนปัจจุบันที่ได้รับอนุญาตตามกฎหมาย ดําเนินการโดยแพทย์ทําการรักษาพยาบาล ตรวจวินิจฉัยโรค และไม่สามารถรับผู้ป่วยไว้ค้างคืนได้ของอาณาเขตนั้น ๆ 

มาตรฐานทางการแพทย์ หมายถึง หลักเกณฑ์หรือแนวทางปฏิบัติทางการแพทย์ที่เป็นสากล และนํามาซึ่งแผนการรักษาที่เหมาะสมกับผู้ป่วยตามความจําเป็นทางการแพทย์ และสอดคล้องกับข้อสรุปจากประวัติการบาดเจ็บ การเจ็บป่วย การตรวจพบ ผลการชันสูตร หรืออื่นๆ (ถ้ามี) 

ความจําเป็นทางการแพทย์ หมายถึง การบริการทางการแพทย์ต่างๆ ที่มีเงื่อนไขดังนี้ 
(1) ต้องสอดคล้องกับการวินิจฉัยโรค และการรักษาตามภาวะการ บาดเจ็บ หรือ การเจ็บป่วยของผู้เอาประกันภัย 
(2) ต้องมีข้อบ่งชี้ทางการแพทย์อย่างชัดเจนตามมาตรฐานเวชปฏิบัติปัจจุบัน
(3) ต้องมิใช่เพื่อความสะดวกของผู้เอาประกันภัย หรือของครอบครัว ผู้เอาประกันภัย หรือของผู้ให้บริการรักษาพยาบาลเพียงฝ่ายเดียวและ 
(4) ต้องเป็นการบริการรักษาพยาบาลตามมาตรฐานการดูแลผู้ป่วยที่ เหมาะสม ตามความจําเป็นของภาวะการบาดเจ็บหรือเจ็บป่วยของผู้เอาประกันภัยนั้นๆ 

แพทย์ทางเลือก หมายถึง การตรวจวินิจฉัยการรักษาพยาบาลหรือการป้องกันโรคโดยวิธีการแพทย์แผนไทย การแพทย์พื้นบ้านไทย การแพทย์แผนจีน หรือวิธีการอื่น ๆ ที่มิใช่การแพทย์แผนปัจจุบัน 

การเข้าพักรักษาตัวครั้งใดครั้งหนึ่ง หมายถึง การต้องอยู่ในโรงพยาบาล เพื่อการรักษาในฐานะผู้ป่วยในครั้งใดครั้งหนึ่ง และให้รวมถึงการต้องอยู่ในโรงพยาบาลสองครั้งหรือมากกว่า ด้วยสาเหตุ หรือโรคหรือภาวะแทรกซ้อนจากโรคเดียวกัน โดยที่ระยะเวลาการต้องอยู่ในโรงพยาบาล แต่ละครั้งห่างกันไม่เกินกว่า 90 วัน นับแต่วันที่ออกจากโรงพยาบาลครั้งสุดท้าย ก็ให้ถือว่าเป็นการเข้าพักรักษาตัวครั้งเดียวกันด้วย 

เอดส์ (AIDS) หมายถึง ภูมิคุ้มกันบกพร่อง (Acquired Immune Deficiency Syndrome) ซึ่งเกิดจากการติดเชื้อไวรัสเอดส์ และให้หมายความรวมถึงการติดเชื้อ จุลชีพฉวยโอกาส เนื้องอกร้ายแรง (Malignant Neoplasm) หรือ การติดโรค หรือการเจ็บป่วยใดๆ ซึ่งโดยผลการตรวจเลือดแสดงเป็น เลือดบวกของไวรัส HIV (Human Immune Deficiency Virus) การติดเชื้อจุลชีพฉวยโอกาส ให้รวมถึง แต่ไม่จํากัดเฉพาะเชื้อที่ทําให้ เกิดโรคปอดบวมหรือปอดอักเสบ (Pneumocystis Carini Pneumonia) เชื้อที่ทําให้เกิดโรคลําไส้อักเสบหรือเรื้อรัง (Organism Or Chronic Enteritis) เชื้อไวรัส (Virus) และ/หรือเชื้อราที่แพร่กระจายอยู่ทั่วไป (Disseminated Fungi Infection) เนื้องอกร้ายแรง (Malignant Neoplasm) ให้รวมถึงแต่ไม่จํากัดเฉพาะเนื้องอก Kaposi's Sarcoma เนื้องอกเซลน้ําเหลืองที่ระบบศูนย์ประสาทส่วนกลาง (Central Nervous System Lymphoma) และ/หรือโรคร้ายแรงอื่นๆ ซึ่งเป็นที่รู้จัก ในปัจจุบันนี้ว่าเป็นอาการของภูมิคุ้มกันบกพร่อง (Acquired Immuno Deficiency Syndrome) หรือซึ่งเป็นสาเหตุที่ทําให้คนที่เป็นเสียชีวิต อย่างกระทันหัน เจ็บป่วย หรือ ทุพพลภาพ โรคภูมิคุ้มกันบกพร่อง (AIDS) ให้รวมถึงเชื้อไวรัส HIV (Human Immuno Deficiency Virus) โรคที่ทำให้สมองเสื่อม Encephalopathy (Dementia) และการ ระบาดของเชื้อไวรัส 

ค่าใช้จ่ายที่จําเป็นและสมควร หมายถึง ค่ารักษาพยาบาล และ/หรือค่าใช้จ่ายใดๆ ที่ควรจะเป็น เมื่อเทียบกับการให้บริการที่โรงพยาบาล เรียกเก็บกับผู้ป่วยทั่วไปของโรงพยาบาลซึ่งผู้เอาประกันภัยเข้ารับการรักษานั้น 

ผลประโยชน์สูงสุดต่อ ปีกรมธรรม์ หมายถึง จํานวนเงินที่ระบุไว้ในตารางผลประโยชน์และเป็นจํานวนเงิน จํากัดความรับผิดสูงสุดภายใต้สัญญาเพิ่มเติมนี้ ที่บริษัทจะจ่ายให้กับผู้เอาประกันภัยรายหนึ่งๆ ในระหว่างปีกรมธรรม์ 

ภาวะที่เป็นมาแต่กําเนิด หมายถึง 
1. ความผิดปกติตั้งแต่แรกเกิด (รวมถึงแต่ไม่จํากัดเพียงความผิดปกติใดๆ ทางร่างกายหรือจิตใจ) หรือ 
2. ความผิดปกติทางร่างกายของเด็กแรกเกิดซึ่งเกิดขึ้นภายในหกเดือน หลังคลอด หรือ
3. ภาวะที่เป็นมาแต่กําเนิดตามการแบ่งประเภทและการจัดกลุ่มขององค์การอนามัยโลก (WHO) เกี่ยวกับรูปร่างผิดปกติแต่กําเนิด การ พิการจนผิดรูปแต่กําเนิด และโครโมโซมผิดปกติรวมถึงการเลื่อนผิดปกติของอวัยวะและโรคลมชัก เว้นแต่มีสาเหตุจากการบาดเจ็บซึ่ง เกิดขึ้นหลังจากวันที่ผู้เอาประกันภัยได้รับความคุ้มครองภายใต้ กรมธรรม์ฉบับนี้อย่างต่อเนื่อง 

การรักษาพยาบาล แบบฉุกเฉิน หมายถึง การเปลี่ยนแปลงทางสุขภาพของผู้เอาประกันภัยอย่างกะทันหันซึ่ง จะต้องได้รับการรักษาอย่างรวดเร็วและเร่งด่วนเพื่อหลีกเลี่ยงการ เสียชีวิตหรือความบกพร่องทางร่างกายของผู้เอาประกันภัยในขณะนั้น

ห้องผู้ป่วยวิกฤต หมายถึง ห้องหรือหน่วยรักษาพยาบาลที่ดูแลผู้ป่วยวิกฤต ดังนี้
ห้องเฝ้าดูอาการ (HDU) คือ หน่วยที่ให้การดูแลรักษาเพิ่มขึ้นกว่า ปรกติ ยกตัวอย่างเช่น ในกรณีที่ระบบการทํางานของอวัยวะหนึ่ง ล้มเหลว 
หน่วยรักษาพยาบาลผู้ป่วยขั้นวิกฤต (IIU/ICU) คือ หน่วยที่ให้ การดูแลรักษาในระดับสูงสุด ยกตัวอย่างเช่น ในกรณีที่ระบบการ ทํางานของอวัยวะหลายระบบล้มเหลว หรือในกรณีที่ต้องใช้เครื่องช่วยหายใจ
หน่วยรักษาพยาบาลผู้ป่วยวิกฤตเฉพาะโรคหัวใจ (CCU) คือ หน่วยที่ให้การดูแลผู้ป่วยด้านหัวใจขั้นสูง

การรักษาทางจิตเวช หมายถึง การรักษาสภาวะทางจิต รวมถึงอาการผิดปกติทางการกิน 

การฟื้นฟูสภาพ หมายถึง การรักษาในรูปของการบําบัดแบบผสมผสาน เช่น กายภาพบําบัด กิจกรรมบําบัดและอรรถบําบัด (การแก้ไขการพูด) มีจุดประสงค์เพื่อ ฟื้นฟูการทํางานของร่างกายทั้งหมดหลังจากการเกิดโรคเฉียบพลัน (เช่น โรคหลอดเลือดสมอง) 

แพทย์เฉพาะทาง หมายถึง แพทย์ซึ่งได้รับการยอมรับจากหน่วยงานภาครัฐที่เกี่ยวข้องในประเทศ ที่ให้การรักษา และมีคุณสมบัติเฉพาะทางในสายงาน หรือมีความ ชํานาญในการรักษาโรคหรือการบาดเจ็บที่ทําการรักษาอยู่

การผ่าตัด หมายถึง ขั้นตอนการทําหัตถการทางการแพทย์ที่มีการใดโดยการใช้มีดผ่าตัดหรือ อุปกรณ์ทางการแพทย์เพื่อการผ่าตัดอื่นลงบางกาย เพื่อทําการรักษาการเจ็บป่วยหรือการบาดเจ็บ รวมถึง การผ่าตัดแบบผู้ป่วยนอก คือ การศัลยกรรมที่ไม่ต้องพักค้างคืน

โรคเรื้อรัง หมายถึง โรคประจําตัวหรืออาการป่วยเรื้อรังที่มีระยะเวลานานกว่า 3 เดือนขึ้นไปและอาจป่วยเรื้อรังต่อไปอย่างไม่มีกําหนด

ภาวะอาการขั้นวิกฤต หมายถึง ภาวะการเจ็บป่วย การบาดเจ็บซึ่งแพทย์เห็นว่ามีอาการรุนแรงมากและต้องการการรักษาเร่งด่วนเพื่อหลีกเลี่ยงการเสียชีวิต หรือการ ทุพพลภาพ หรือเป็นภัยต่อสุขภาพในระยะยาวซึ่งระดับความรุนแรง ของภาวะอาการจะพิจารณาจาก ปัจจัยที่เกี่ยวเนื่องกับสถานที่ที่ ผู้เอาประกันภัยอยู่ ณ ขณะนั้น ลักษณะความฉุกเฉินทางการแพทย์ และการจัดหาเครื่องมืออุปกรณ์ทางการแพทย์ที่เหมาะสมต่อการรักษาพยาบาล

พื้นที่ความคุ้มครอง หมายถึง พื้นที่ความคุ้มครองตามผลประโยชน์ความคุ้มครองของผู้เอาประกันภัย ซึ่งบริษัทฯ ได้กําหนดขึ้นทั้งหมด 4 แห่ง โดย ผู้เอาประกันภัยต้องเลือกแห่งใดแห่งหนึ่งดังต่อไปนี้ และบริษัทฯ จะ ระบุแห่งที่ผู้เอาประกันภัยเลือกไว้ในตารางผลประโยชน์ 
(1) เฉพาะประเทศไทย 
(2) ทวีปเอเชีย ได้แก่ บังกลาเทศ ภูฏาน บรูไน กัมพูชา จีน ฮ่องกง อินเดีย อินโดนีเซีย ญี่ปุ่น คาซัคสถาน คีร์กีซสถาน ลาว มาเก๊า มาเลเซีย มัลดีฟส์ มองโกเลีย พม่า เนปาล ปากีสถาน ฟิลิปปินส์ สิงคโปร์ เกาหลีใต้ ศรีลังกา ไต้หวัน ทาจิกิสถาน เติร์กเมนิสถาน อุซเบกิสถาน เวียดนามและไทย 
(3) ทั่วโลกยกเว้นสหรัฐอเมริกาและเกาะเล็กรอบนอกของ สหรัฐอเมริกา
(4) ทั่วโลก

ประเทศภูมิลําเนา (Home Country) หมายถึง ประเทศที่ผู้เอาประกันภัยถือสัญชาติ

ประเทศที่พํานักอยู่เป็นปกติ (Usual Country of Residence) หมายถึง ประเทศที่ผู้เอาประกันภัยมีที่อยู่อาศัยเป็นการถาวร

ที่พักอาศัย (Place of Residence) หมายถึง ที่อยู่อาศัยขณะปัจจุบันตามที่แถลงโดยผู้เอาประกันภัย